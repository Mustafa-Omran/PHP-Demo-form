/* 
Form Script : Mustafa Omran
 */

/* onclick */

var your_name = document.getElementById("name");

your_name.onfocus = function() 
{
	if ( your_name.value === "Enter your name") 
        {
		your_name.value = "";
                document.getElementById("name").style.backgroundColor="orange";
	}
};

your_name.onblur = function()
{
	if ( your_name.value === "") 
        {
		your_name.value = "Enter your name";
                document.getElementById("name").style.backgroundColor="#cccccc";
	}
};

var your_email = document.getElementById("email");

your_email.onfocus = function() 
{
	if ( your_email.value === "Enter your e-mail") 
        {
		your_email.value = "";
                document.getElementById("email").style.backgroundColor="orange";
	}
};

your_email.onblur = function() 
{
	if ( your_email.value === "") 
        {
		your_email.value = "Enter your e-mail";
                document.getElementById("email").style.backgroundColor="#cccccc";
	}
};

var pass = document.getElementById("password");

pass.onfocus = function() 
{
    pass.style.backgroundColor="orange";
};

pass.onblur = function() 
{
   pass.style.backgroundColor="#cccccc";
};

var re_pass = document.getElementById("repeated_password");

re_pass.onfocus = function() 
{
    re_pass.style.backgroundColor="orange";
};

re_pass.onblur = function() 
{
   re_pass.style.backgroundColor="#cccccc";
};


function change_clr(){
    document.getElementById("change_color").style.color="black";
    document.getElementById("change_color").style.backgroundColor="orange";
    
}


