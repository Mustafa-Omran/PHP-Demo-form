<!DOCTYPE html>
                                       <!-- Mustafa Omran -->
                                <!--  form validation using PHP-->
        
        <html>
            <head>
                <title>Demo Form</title>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <link href="css/style.css" rel="stylesheet">

                <!-- font awesome library -->
                <link href="css/font-awesome.min.css" rel="stylesheet">
                
                <!-- w3.css library for responsive design-->
                <link href="css/w3.css" rel="stylesheet">
            </head>
            <body class="w3-animate-zoom w3-row">
             <!-- header -->
                <div>
                    <div class="w3-orange w3-text-white w3-xxlarge w3-center">
                        <i class="fa fa-forumbee w3-animate-zoom"></i> 
                        <i class="fa fa-forumbee w3-animate-fading"></i> 
                        <i class="fa fa-forumbee w3-animate-zoom"></i> 
                        Demo Form
                    </div>
                </div>

                <!-- form body -->
                <p class="w3-center w3-text-red w3-light-grey w3-large"> 
                 <?php 
                 require './files/validate.php';
                  ?>
                    
                </p>
                <div class="w3-light-grey">
                   
   
                    <form id="demo_form" action="index.php" method="post"class="w3-form w3-large">
                       Your name :                             
                       <input type="text" value="Enter your name" id="name" name="username"  class="w3-input w3-border w3-border-orange w3-round"><br>
                       E-mail :         
                       <input type="email" value="Enter your e-mail" id="email" name="usermail" class="w3-input w3-border w3-border-orange w3-round"><br>
                       Password :       
                       <input type="password" id="password" name="password" required class="w3-input w3-border w3-border-orange w3-round"><br>
                       Repeat password :
                       <input type="password" id="repeated_password" name="repeated_password" required class="w3-input w3-border w3-border-orange w3-round"><br>
                       Select governorate 
                       <select class="w3-select w3-round w3-border w3-border-orange" name="usergovernorate">
                           <?php
                           foreach ($governorates as $governorate)
                               {
                               echo "<option>{$governorate}</option>";
                               
                               } 
                               ?>
                       </select>
                       Select Gender :  <br>
                       <input type="radio" name="gender" value="Male" id="male" class="w3-radio w3-border">Male <br>
                       <input type="radio"  name="gender" value="Female" id="female" class="w3-radio">Female
                       <br><br>

                       Select your Birth date :<br>  

                       <select class="w3-select w3-round w3-border w3-border-orange" name="userbirthdate">

                                       <?php 
                                       foreach ($selectitems as $selectitem)
                                       {
                                       echo "<option>{$selectitem}</option>";
                                       }
                                     
                                        ?>
                           </select>
                                      <?php echo '<br>'; ?>
                                      <br>
                  <input type="submit" value="Send" name="submit" class="w3-btn w3-orange w3-hover-grey w3-text-white w3-hover-text-white w3-round">

          

                    </form>
                </div>
                <!-- footer -->
                <div class="w3-center w3-xlarge w3-orange w3-text-white">
                     Mustafa Omran <i class="fa fa-copyright"></i> <?php echo date('Y'); ?> 
                </div>
                <script src="js/script.js"></script>
            </body>
        </html>