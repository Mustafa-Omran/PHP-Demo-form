<?php

/* 
 *                             Mustafa Omran 
 *                          validate user info @ 2017
 */

   // select as array
   $selectitems=array('year',1970,1971,1972,1973,1974,1975,1976,1977,1978,1979,1980,1981,1982,1983,1984,1985,1986,1987,1988,1989,1990,1991,1992,1993,1994,1995,1996,1997,1998,1999,2000);
   $governorates=array('governorate','Cairo','Giza','Alex','Minia','Assuit','Aswan','Qena','Suhaj','Luxor','Red sea','Mansuora','Octobar','Wadi jadeed','Sharqya','Alareesh','Mars matrouh','Sharm shiekh','Sinai');



              $value=$_POST['submit'];
                if(!isset($value) || empty($value))
                    {
                    echo "please complete all fields !".' '.'<span class="w3-text-green fa fa-pencil w3-large"></span>';
                    }
                   else {
                       // validate name
                       $value=$_POST['username'];
                       $min=10;
                       $max=30;
                       
                       if(strlen($value)<$min || strlen($value)>$max)
                           {
                           echo "you must enter user name between 10:30 chars".' '.'<span class="w3-text-green fa fa-thumbs-o-down w3-large"></span>'.'<br>';
                           }
                           else 
                           { 
                           $username=$_POST['username'];
                           echo "username: {$username}".' '.'<span class="w3-text-green fa fa-thumbs-o-up w3-large"></span>'.'<br>';
                           }
                           
                           // validate user e-mail !
                           $value=$_POST['usermail'];
                           if(preg_match("/@/", $value))
                               {
                               $usermail=$_POST['usermail'];
                               echo "usermail: {$usermail}".' '.'<span class="w3-text-green fa fa-thumbs-o-up w3-large"></span>'.'<br>';
                               }
                               else 
                               {
                               echo "you must enter a valid e-mail example@example.com".' '.'<span class="w3-text-green fa fa-thumbs-o-down w3-large"></span>'.'<br>';
                               }
                               
                              $password=$_POST['password'];
                              $repeated_password=$_POST['repeated_password'];
                              
                              if(!($password===$repeated_password))
                                  {
                                  echo "you must match two passwords".' '.'<span class="w3-text-green fa fa-thumbs-o-down w3-large"></span>'.'<br>';
                                  }
                                  else 
                                  {
                                  $password=$_POST['password'];
                                // echo "userpassword: {$password}";
                                  }
                                 
                                 
                           // check gender
                               $gender=$_POST['gender'];
                               if(isset($gender) && $gender=="Male")
                                   {
                                   echo "Male checked".' '.'<span class="w3-text-green fa fa-thumbs-o-up w3-large"></span>'.'<br>';
                                   }
                                   elseif(isset ($gender) && $gender=="Female") 
                                  {
                                  echo "Female checked".' '.'<span class="w3-text-green fa fa-thumbs-o-up w3-large"></span>'.'<br>';
                                  } 
                                  else
                                  {
                                  echo "you must select gender !".' '.'<span class="w3-text-green fa fa-thumbs-o-down w3-large"></span>'.'<br>';
                                  }
                                  
                                      // validate user birthdate // validate user governorate
                                      $userdate=$_POST['userbirthdate'];
                                      if((!isset($userdate) || !empty($userdate) || (!isset($usergovernorate)) || !empty($usergovernorate)))
                                      {
                                      $userdate=$_POST['userbirthdate'];
                                      $usergovernorate=$_POST['usergovernorate'];
                                      if($userdate==='year')
                                          {
                                          echo 'you must select your year'.' '.'<span class="w3-text-green fa fa-thumbs-o-down w3-large"></span>'.'<br>';
                                          }
                                      else
                                          {
                                          echo $userdate.' '.'<span class="w3-text-green fa fa-thumbs-o-up w3-large"></span>'.'<br>';
                                          }
                                      if($usergovernorate==='governorate')
                                          {
                                          echo 'you must select your governorate'.' '.'<span class="w3-text-green fa fa-thumbs-o-down w3-large"></span>'.'<br>';
                                          }
                                          else
                                          {
                                           echo $usergovernorate.' '.'<span class="w3-text-green fa fa-thumbs-o-up w3-large"></span>'.'<br>';   
                                          }
                                      
                                      }                           
                           }
?>